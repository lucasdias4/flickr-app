package com.lucasdias.search.domain.usecase

import com.lucasdias.search.domain.repository.SearchHistoryRepository

class GetSearchHistory(private val searchHistoryRepository: SearchHistoryRepository) {
    fun execute() = searchHistoryRepository.getHistory()
}
