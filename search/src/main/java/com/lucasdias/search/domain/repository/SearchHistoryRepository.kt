package com.lucasdias.search.domain.repository

import androidx.lifecycle.LiveData

interface SearchHistoryRepository {
    fun setSearch(search: String)
    fun getHistory(): LiveData<ArrayList<String>>
}
