package com.lucasdias.search.domain.usecase

import com.lucasdias.search.domain.repository.SearchHistoryRepository

class SetSearchHistory(private val searchHistoryRepository: SearchHistoryRepository) {
    fun execute(search: String) {
        searchHistoryRepository.setSearch(search)
    }
}
