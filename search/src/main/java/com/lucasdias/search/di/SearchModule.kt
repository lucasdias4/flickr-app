package com.lucasdias.search.di

import com.lucasdias.search.data.SearchHistoryRepositoryImpl
import com.lucasdias.search.domain.repository.SearchHistoryRepository
import com.lucasdias.search.domain.usecase.GetSearchHistory
import com.lucasdias.search.domain.usecase.SetSearchHistory
import com.lucasdias.search.presentation.SearchAdapter
import com.lucasdias.search.presentation.SearchViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val searchModule = module {

    viewModel {
        SearchViewModel(
                get<GetSearchHistory>(),
                get<SetSearchHistory>()
        )
    }

    factory { (methdod: ((String) -> Unit)?) ->
        SearchAdapter(methdod)
    }

    factory {
        GetSearchHistory(get<SearchHistoryRepository>())
    }

    factory {
        SetSearchHistory(get<SearchHistoryRepository>())
    }

    factory {
        SearchHistoryRepositoryImpl() as SearchHistoryRepository
    }
}
