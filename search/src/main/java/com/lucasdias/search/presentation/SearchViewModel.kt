package com.lucasdias.search.presentation

import androidx.lifecycle.ViewModel
import com.lucasdias.search.domain.usecase.GetSearchHistory
import com.lucasdias.search.domain.usecase.SetSearchHistory

class SearchViewModel(
    private val getSearchHistory: GetSearchHistory,
    private val setSearchHistory: SetSearchHistory
) : ViewModel() {

    fun getHistory() = getSearchHistory.execute()

    fun setSearch(search: String) {
        setSearchHistory.execute(search)
    }
}
