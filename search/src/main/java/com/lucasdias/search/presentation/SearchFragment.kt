package com.lucasdias.search.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textfield.TextInputEditText
import com.lucasdias.baseextensions.bind
import com.lucasdias.search.R
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class SearchFragment(private val searchClickMethod: (String) -> Unit?) : Fragment() {

    companion object {
        fun newInstance(searchClickMethod: (String) -> Unit?) =
                SearchFragment(searchClickMethod)
    }

    private val viewModel by inject<SearchViewModel>()
    private val adapter: SearchAdapter by inject { parametersOf(searchClickMethod) }
    private val recyclerView by bind<RecyclerView>(R.id.search_history_list)
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private val searchButton by bind<Button>(R.id.search_button_to_search)
    private val inputTextArea by bind<TextInputEditText>(R.id.search_input_edit_text)
    private var hasNetworkConnectivity = true

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
        initSearchButtonListener()
        initRecyclerView()
    }

    fun updateConnectivityInformation(hasNetworkConnectivity: Boolean) {
        this.hasNetworkConnectivity = hasNetworkConnectivity
    }

    private fun initRecyclerView() {
        layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recyclerView?.setHasFixedSize(true)
        recyclerView?.layoutManager = layoutManager
        recyclerView?.adapter = adapter
    }

    private fun initSearchButtonListener() {
        searchButton?.setOnClickListener {
            val inputText = inputTextArea?.text.toString()
            viewModel.setSearch(inputText)
            searchClickMethod(inputText)
        }
    }

    private fun initObservers() {
        viewModel.apply {
            getHistory().observe(viewLifecycleOwner, Observer { searchHistory ->
                adapter.updateSearchTextList(searchHistory)
            })
        }
    }
}
