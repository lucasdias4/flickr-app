package com.lucasdias.search.data

import com.lucasdias.search.domain.repository.SearchHistoryRepository

class SearchHistoryRepositoryImpl : SearchHistoryRepository {

    override fun getHistory() = SearchHistoryCache.getHistory()

    override fun setSearch(search: String) {
        SearchHistoryCache.setSearch(search)
    }
}
