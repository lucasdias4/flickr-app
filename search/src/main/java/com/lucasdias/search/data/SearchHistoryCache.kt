package com.lucasdias.search.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

object SearchHistoryCache {
    private val searchHistory = ArrayList<String>()

    private val history = MutableLiveData<ArrayList<String>>()
    fun getHistory(): LiveData<ArrayList<String>> = history

    fun setSearch(search: String) {
        searchHistory.add(search)
        history.postValue(searchHistory)
    }
}
