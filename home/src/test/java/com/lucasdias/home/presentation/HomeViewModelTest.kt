package com.lucasdias.home.presentation

import androidx.lifecycle.MutableLiveData
import io.mockk.*
import org.junit.Test

class HomeViewModelTest {

    private companion object {
        const val IS_INITIALIZED = true
        const val IS_NOT_INITIALIZED = false
    }

    val homeViewModel = spyk(HomeViewModel())

    @Test
    fun `GIVEN needs a PhotoDetailFragment WHEN call a new fragment with PhotoDetailEnum THEN calls a PhotoDetailFragment start method`() {
        every {
            homeViewModel.startPhotoDetailFragment(any())
        } just Runs

        homeViewModel.startFragment(
                IS_INITIALIZED,
                HomeViewModel.ViewFragmentsEnum.PHOTO_DETAIL_FRAGMENT
        )

        verify(exactly = 1) {
            homeViewModel.startPhotoDetailFragment(any())
        }

        verify(exactly = 0) {
            homeViewModel.startSearchFragment(any())
        }
    }

    @Test
    fun `GIVEN needs a SearchFragment WHEN call a new fragment with SearchEnum THEN calls a SearchFragment start method`() {
        every {
            homeViewModel.startSearchFragment(any())
        } just Runs

        homeViewModel.startFragment(IS_INITIALIZED, HomeViewModel.ViewFragmentsEnum.SEARCH_FRAGMENT)

        verify(exactly = 1) {
            homeViewModel.startSearchFragment(any())
        }

        verify(exactly = 0) {
            homeViewModel.startPhotoDetailFragment(any())
        }
    }

    @Test
    fun `GIVEN needs a SearchFragment WHEN SearchFragment already been initialized THEN calls method to change to SearchFragment`() {
        val mockedChangeToSearchFragment: MutableLiveData<Unit> = mockk()
        val mockedAddANewSearchFragment: MutableLiveData<Unit> = mockk()

        every {
            mockedChangeToSearchFragment.postValue(any())
        } just Runs

        homeViewModel.changeToSearchFragment = mockedChangeToSearchFragment
        homeViewModel.addANewSearchFragment = mockedAddANewSearchFragment

        homeViewModel.startSearchFragment(IS_INITIALIZED)

        verify(exactly = 1) {
            mockedChangeToSearchFragment.postValue(any())
        }

        verify(exactly = 0) {
            mockedAddANewSearchFragment.postValue(any())
        }
    }

    @Test
    fun `GIVEN needs a SearchFragment WHEN SearchFragment have not been initialized THEN calls method to create and change to SearchFragment`() {
        val mockedChangeToSearchFragment: MutableLiveData<Unit> = mockk()
        val mockedAddANewSearchFragment: MutableLiveData<Unit> = mockk()

        every {
            mockedAddANewSearchFragment.postValue(any())
        } just Runs

        homeViewModel.changeToSearchFragment = mockedChangeToSearchFragment
        homeViewModel.addANewSearchFragment = mockedAddANewSearchFragment

        homeViewModel.startSearchFragment(IS_NOT_INITIALIZED)

        verify(exactly = 0) {
            mockedChangeToSearchFragment.postValue(any())
        }

        verify(exactly = 1) {
            mockedAddANewSearchFragment.postValue(any())
        }
    }

    @Test
    fun `GIVEN needs a PhotoDetailFragment WHEN PhotoDetailFragment already been initialized THEN calls method to change to PhotoDetailFragment`() {
        val mockedChangeToPhotoDetailFragment: MutableLiveData<Unit> = mockk()
        val mockedAddANewPhotoDetailFragment: MutableLiveData<Unit> = mockk()

        every {
            mockedChangeToPhotoDetailFragment.postValue(any())
        } just Runs

        homeViewModel.changeToSearchFragment = mockedChangeToPhotoDetailFragment
        homeViewModel.addANewSearchFragment = mockedAddANewPhotoDetailFragment

        homeViewModel.startSearchFragment(IS_INITIALIZED)

        verify(exactly = 1) {
            mockedChangeToPhotoDetailFragment.postValue(any())
        }

        verify(exactly = 0) {
            mockedAddANewPhotoDetailFragment.postValue(any())
        }
    }

    @Test
    fun `GIVEN needs a PhotoDetailFragment WHEN PhotoDetailFragment have not been initialized THEN calls method to create and change to PhotoDetailFragment`() {
        val mockedChangeToPhotoDetailFragment: MutableLiveData<Unit> = mockk()
        val mockedAddANewPhotoDetailFragment: MutableLiveData<Unit> = mockk()

        every {
            mockedAddANewPhotoDetailFragment.postValue(any())
        } just Runs

        homeViewModel.changeToSearchFragment = mockedChangeToPhotoDetailFragment
        homeViewModel.addANewSearchFragment = mockedAddANewPhotoDetailFragment

        homeViewModel.startSearchFragment(IS_NOT_INITIALIZED)

        verify(exactly = 0) {
            mockedChangeToPhotoDetailFragment.postValue(any())
        }

        verify(exactly = 1) {
            mockedAddANewPhotoDetailFragment.postValue(any())
        }
    }
}
