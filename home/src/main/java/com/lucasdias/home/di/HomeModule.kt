package com.lucasdias.home.di

import com.lucasdias.home.presentation.HomeViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val homeModule = module {
    viewModel {
        HomeViewModel()
    }
}

val searchModule = com.lucasdias.search.di.searchModule
