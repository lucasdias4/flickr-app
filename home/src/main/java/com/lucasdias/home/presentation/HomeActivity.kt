package com.lucasdias.home.presentation

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar
import com.lucasdias.baseextensions.bind
import com.lucasdias.bottomnavigation.BottomNavigation
import com.lucasdias.bottomnavigation.model.BottomNavigationOption
import com.lucasdias.connectivity.Connectivity
import com.lucasdias.home.R
import com.lucasdias.photocatalog.presentation.PhotoCatalogFragment
import com.lucasdias.photodetail.PhotoDetailActivity
import com.lucasdias.photodetail.PhotoDetailActivity.Companion.URL_KEY
import com.lucasdias.search.presentation.SearchFragment
import org.koin.android.viewmodel.ext.android.viewModel

class HomeActivity : AppCompatActivity() {

    private val viewModel by viewModel<HomeViewModel>()
    private val fragmentContainer by bind<FrameLayout>(R.id.home_activity_fragment_container)
    private val bottomNavigationContainer: FrameLayout by lazy {
        findViewById<FrameLayout>(R.id.home_activity_bottom_navigation_container)
    }
    private var hasNetworkConnectivity = true
    private lateinit var searchFragment: SearchFragment
    private lateinit var photoCatalogFragment: PhotoCatalogFragment
    private lateinit var connectivity: Connectivity
    private lateinit var snackbar: Snackbar
    private lateinit var snackbarText: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        startPhotoCatalogFragment()
        initBottomNavigation()
        initConnectivityCallback()
        initConnectivitySnackbar()
        initFragmentsObservers()
        initConnectivityObserver()
    }

    private fun startPhotoCatalogFragment() {
        val isInitialized: Boolean = ::photoCatalogFragment.isInitialized
        viewModel.startFragment(
            isInitialized,
            HomeViewModel.ViewFragmentsEnum.PHOTO_DETAIL_FRAGMENT
        )
    }

    private fun startSearchFragment() {
        val isInitialized: Boolean = ::searchFragment.isInitialized
        viewModel.startFragment(isInitialized, HomeViewModel.ViewFragmentsEnum.SEARCH_FRAGMENT)
    }

    private fun initBottomNavigation() {
        val menuFirstPostion = 1
        val menuSecondPostion = 2

        val bottomNavigationBackgroundColor = getColor(R.color.purple)

        val resultOptionTitle = resources.getString(R.string.bottom_navigation_result)
        val resultOptionClickAction = { startPhotoCatalogFragment() }
        val resultOptionIcon = resources.getDrawable(
            R.drawable.bottom_navigation_result_icon,
            null
        )

        val searchOptionTitle = resources.getString(R.string.bottom_navigation_search)
        val searchOptionClickAction = { startSearchFragment() }
        val searchOptionIcon = resources.getDrawable(
            R.drawable.bottom_navigation_search_icon,
            null
        )

        val bottomNavigationOptionList = ArrayList<BottomNavigationOption>()

        bottomNavigationOptionList.add(
            BottomNavigationOption(
                BottomNavigationEnum.RESULT.id,
                resultOptionTitle,
                resultOptionIcon,
                menuFirstPostion,
                resultOptionClickAction
            )
        )
        bottomNavigationOptionList.add(
            BottomNavigationOption(
                BottomNavigationEnum.SEARCH.id,
                searchOptionTitle,
                searchOptionIcon,
                menuSecondPostion,
                searchOptionClickAction
            )
        )

        BottomNavigation.init(
            this@HomeActivity,
            bottomNavigationBackgroundColor,
            bottomNavigationContainer,
            bottomNavigationOptionList
        )
    }

    private fun initConnectivitySnackbar() {
        snackbar =
            Snackbar.make(fragmentContainer, getString(R.string.connectivity_on_snackbar), Snackbar.LENGTH_INDEFINITE)
        snackbarText = snackbar.view.findViewById(R.id.snackbar_text)
        snackbarText.textAlignment = View.TEXT_ALIGNMENT_CENTER
    }

    private fun initConnectivityCallback() {
        connectivity = Connectivity(application)
    }

    private fun initPhotoDetailActivity(url: String) {
        val intent = Intent(this, PhotoDetailActivity::class.java).apply {
            putExtra(URL_KEY, url)
        }
        startActivity(intent)
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
    }

    private fun initFragmentsObservers() {
        viewModel.apply {
            addANewPhotoCatalogFragment().observe(this@HomeActivity, Observer {
                val photoClickMethod = { url: String ->
                    initPhotoDetailActivity(url)
                }
                photoCatalogFragment = PhotoCatalogFragment.newInstance(photoClickMethod)
                addFragment(photoCatalogFragment as Fragment)
            })

            changeToPhotoCatalogFragment().observe(this@HomeActivity, Observer {
                changeFragment(photoCatalogFragment)
                photoCatalogFragment.verifyIfItIsTheUsersFirstTime()
            })

            addANewSearchFragment().observe(this@HomeActivity, Observer {
                val searchActionMethod = { searchText: String ->
                    selectBottomNavigationOptionProgrammatically(BottomNavigationEnum.RESULT.id)
                    photoCatalogFragment.updateSearch(searchText)
                }

                searchFragment = SearchFragment.newInstance(searchActionMethod)
                addFragment(searchFragment as Fragment)
            })

            changeToSearchFragment().observe(this@HomeActivity, Observer {
                changeFragment(searchFragment)
            })
        }
    }

    private fun initConnectivityObserver() {
        connectivity.observe(this@HomeActivity, Observer { hasNetworkConnectivity ->
            this.hasNetworkConnectivity = hasNetworkConnectivity
            val isPhotoCatalogFragmentInitialized: Boolean = ::photoCatalogFragment.isInitialized
            val isSearchFragmentInitialized: Boolean = ::searchFragment.isInitialized

            viewModel.mustShowConnectivitySnackbar(hasNetworkConnectivity)
            viewModel.updateFragmentsConnectivityInformation(
                isPhotoCatalogFragmentInitialized,
                isSearchFragmentInitialized
            )
        })

        viewModel.apply {
            updatePhotoCatalogFragmentConnectivity().observe(this@HomeActivity, Observer {
                photoCatalogFragment.updateConnectivityInformation(hasNetworkConnectivity)
            })

            updateSearchFragmentConnectivity().observe(this@HomeActivity, Observer {
                searchFragment.updateConnectivityInformation(hasNetworkConnectivity)
            })

            showConnectivityOnSnackbar().observe(this@HomeActivity, Observer {
                this@HomeActivity.showConnectivityOnSnackbar()
            })

            showConnectivityOffSnackbar().observe(this@HomeActivity, Observer {
                this@HomeActivity.showConnectivityOffSnackbar()
            })
        }
    }

    private fun changeFragment(fragment: Fragment?) {
        fragment?.let {
            hideAllFragments()
            showSelectedFragment(fragment)
        }
    }

    private fun showSelectedFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .show(fragment)
            .commit()
    }

    private fun hideAllFragments() {
        val fragmentList = supportFragmentManager.fragments

        fragmentList.forEach { fragment ->
            supportFragmentManager
                .beginTransaction()
                .hide(fragment)
                .commit()
        }
    }

    private fun addFragment(fragment: Fragment) {
        hideAllFragments()

        supportFragmentManager
            .beginTransaction()
            .add(R.id.home_activity_fragment_container, fragment)
            .commit()
    }

    private fun showConnectivityOnSnackbar() {
        snackbar.duration = Snackbar.LENGTH_LONG
        snackbar.view.setBackgroundColor(ContextCompat.getColor(this, R.color.green))
        snackbar.setText(getString(R.string.connectivity_on_snackbar))
        snackbar.show()
    }

    private fun showConnectivityOffSnackbar() {
        snackbar.duration = Snackbar.LENGTH_INDEFINITE
        snackbar.view.setBackgroundColor(ContextCompat.getColor(this, R.color.red))
        snackbar.setText(getString(R.string.connectivity_off_snackbar))
        snackbar.show()
    }

    private fun selectBottomNavigationOptionProgrammatically(bottomNavigationOptionId: Int) {
        val bottomNavigationView: BottomNavigationView? = BottomNavigation.getBottomNavigationView()
        bottomNavigationView?.selectedItemId = bottomNavigationOptionId
    }

    private enum class BottomNavigationEnum(val id: Int) {
        RESULT(1),
        SEARCH(2)
    }
}
