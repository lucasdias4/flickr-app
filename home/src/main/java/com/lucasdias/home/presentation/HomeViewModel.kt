package com.lucasdias.home.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class HomeViewModel : ViewModel() {

    internal var addANewPhotoCatalogFragment = MutableLiveData<Unit>()
    internal var changeToPhotoCatalogFragment = MutableLiveData<Unit>()
    internal var addANewSearchFragment = MutableLiveData<Unit>()
    internal var changeToSearchFragment = MutableLiveData<Unit>()
    internal var updatePhotoCatalogFragmentConnectivity = MutableLiveData<Unit>()
    internal var updateSearchFragmentConnectivity = MutableLiveData<Unit>()
    internal var showConnectivityOnSnackbar = MutableLiveData<Unit>()
    internal var showConnectivityOffSnackbar = MutableLiveData<Unit>()
    private var wasConnected = true

    fun addANewPhotoCatalogFragment(): LiveData<Unit> = addANewPhotoCatalogFragment
    fun changeToPhotoCatalogFragment(): LiveData<Unit> = changeToPhotoCatalogFragment
    fun addANewSearchFragment(): LiveData<Unit> = addANewSearchFragment
    fun changeToSearchFragment(): LiveData<Unit> = changeToSearchFragment
    fun showConnectivityOnSnackbar(): LiveData<Unit> = showConnectivityOnSnackbar
    fun showConnectivityOffSnackbar(): LiveData<Unit> = showConnectivityOffSnackbar
    fun updatePhotoCatalogFragmentConnectivity(): LiveData<Unit> =
        updatePhotoCatalogFragmentConnectivity
    fun updateSearchFragmentConnectivity(): LiveData<Unit> =
        updateSearchFragmentConnectivity

    fun startFragment(isInitialized: Boolean, viewFragmentsEnum: ViewFragmentsEnum) {
        if (viewFragmentsEnum == ViewFragmentsEnum.PHOTO_DETAIL_FRAGMENT) startPhotoDetailFragment(
                isInitialized
        )
        else startSearchFragment(isInitialized)
    }

    internal fun startPhotoDetailFragment(isCreated: Boolean) {
        if (isCreated) changeToPhotoCatalogFragment.postValue(Unit)
        else addANewPhotoCatalogFragment.postValue(Unit)
    }

    internal fun startSearchFragment(isCreated: Boolean) {
        if (isCreated) changeToSearchFragment.postValue(Unit)
        else addANewSearchFragment.postValue(Unit)
    }

    fun updateFragmentsConnectivityInformation(
        photoCatalogFragmentInitialized: Boolean,
        searchFragmentInitialized: Boolean
    ) {
        if (photoCatalogFragmentInitialized)
            updatePhotoCatalogFragmentConnectivity.postValue(Unit)
        if (searchFragmentInitialized)
            updateSearchFragmentConnectivity.postValue(Unit)
    }

    fun mustShowConnectivitySnackbar(hasNetworkConnectivity: Boolean) {
        if (hasNetworkConnectivity.not()) {
            showConnectivityOffSnackbar.postValue(Unit)
            wasConnected = false
        } else if (wasConnected.not() && hasNetworkConnectivity) {
            showConnectivityOnSnackbar.postValue(Unit)
            wasConnected = true
        }
    }

    enum class ViewFragmentsEnum(id: Int) {
        PHOTO_DETAIL_FRAGMENT(1),
        SEARCH_FRAGMENT(2)
    }
}
