package com.lucasdias.flickrapplication

import android.app.Application
import com.lucasdias.home.di.homeModule
import com.lucasdias.home.di.searchModule
import com.lucasdias.log.AppLog
import com.lucasdias.photocatalog.di.photoCatalogModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.logger.AndroidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.EmptyLogger
import org.koin.core.logger.Logger

class Application : Application() {

    override fun onCreate() {
        super.onCreate()

        AppLog.initialize(BuildConfig.DEBUG)

        startKoin {
            androidContext(this@Application)
            logger(koinLogger())
            modules(
                listOf(
                    homeModule,
                    photoCatalogModule,
                    searchModule
                )
            )
        }
    }

    private fun koinLogger(): Logger = if (BuildConfig.DEBUG) AndroidLogger() else EmptyLogger()
}
