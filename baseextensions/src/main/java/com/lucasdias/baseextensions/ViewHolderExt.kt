package com.lucasdias.baseextensions

import android.view.View
import androidx.annotation.IdRes
import androidx.recyclerview.widget.RecyclerView

fun <T : View> RecyclerView.ViewHolder.bind(view: View, @IdRes id: Int): Lazy<T> {
    return lazy { view.findViewById<T>(id) }
}
