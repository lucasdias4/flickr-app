package com.lucasdias.photodetail

import android.os.Bundle
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import com.lucasdias.baseextensions.bind
import com.lucasdias.baseextensions.gone
import com.lucasdias.baseextensions.loadUrl
import com.lucasdias.baseextensions.toast

class PhotoDetailActivity : AppCompatActivity() {

    companion object {
        const val URL_KEY = "URL_KEY"
    }

    private val photo by bind<ImageView>(R.id.photo_detail_activity_image)
    private val photoLoadingProgressBar by bind<ProgressBar>(R.id.photo_detail_loading_progress_bar)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo_detail)
        val url = intent.getStringExtra(URL_KEY)
        val onImageLoaded = { onImageLoaded() }
        val onImageLoadingError = { onImageLoadingError() }
        photo.loadUrl(url = url, onLoadCompleted = onImageLoaded, onError = onImageLoadingError)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }

    private fun onImageLoaded() {
        photoLoadingProgressBar.gone()
    }

    private fun onImageLoadingError() {
        val onErrorMessage: CharSequence = getString(R.string.photo_detail_error_message)
        this.toast(onErrorMessage)
        photoLoadingProgressBar.gone()
    }
}
