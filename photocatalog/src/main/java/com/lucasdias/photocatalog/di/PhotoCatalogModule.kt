package com.lucasdias.photocatalog.di

import androidx.room.Room
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.lucasdias.photocatalog.data.paging.PagingRepositoryImpl
import com.lucasdias.photocatalog.data.paging.local.PagingCache
import com.lucasdias.photocatalog.data.photocatalog.PhotoCatalogRepositoryImpl
import com.lucasdias.photocatalog.data.photocatalog.local.PhotoDao
import com.lucasdias.photocatalog.data.photocatalog.local.PhotoDatabase
import com.lucasdias.photocatalog.data.photocatalog.remote.PhotoCatalogService
import com.lucasdias.photocatalog.data.searchtext.SearchTextRepositoryImpl
import com.lucasdias.photocatalog.data.searchtext.local.SearchTextCache
import com.lucasdias.photocatalog.data.userfirsttime.UserFirstTimeRepositoryImpl
import com.lucasdias.photocatalog.data.userfirsttime.local.UserFirstTimeCache
import com.lucasdias.photocatalog.domain.repository.PagingRepository
import com.lucasdias.photocatalog.domain.repository.PhotoCatalogRepository
import com.lucasdias.photocatalog.domain.repository.SearchTextRepository
import com.lucasdias.photocatalog.domain.repository.UserFirstTimeRepository
import com.lucasdias.photocatalog.domain.usecase.*
import com.lucasdias.photocatalog.presentation.PhotoCatalogAdapter
import com.lucasdias.photocatalog.presentation.PhotoCatalogViewModel
import com.lucasdias.photocatalog.utils.Constants.Companion.PHOTO_CATALOG_API_BASE_URL
import com.lucasdias.photocatalog.utils.Constants.Companion.PHOTO_DATABASE
import java.util.concurrent.TimeUnit
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val photoCatalogModule = module {
    viewModel {
        PhotoCatalogViewModel(
                get<SearchPhotosByTagAndPageFromApi>(),
                get<GetPhotosFromDatabase>(),
                get<GetActualPage>(),
                get<SetActualPage>(),
                get<GetActualSearchText>(),
                get<SetActualSearchText>(),
                get<DeleteAllPhotos>(),
                get<GetIfIsTheUsersFirstTime>(),
                get<SetThatIsNotTheUsersFirstTime>()
        )
    }
    factory { (methdod: ((String) -> Unit)?) ->
        PhotoCatalogAdapter(methdod)
    }

    factory {
        SearchPhotosByTagAndPageFromApi(
                get<PhotoCatalogRepository>()
        )
    }

    factory {
        GetPhotosFromDatabase(
                get<PhotoCatalogRepository>()
        )
    }

    factory {
        DeleteAllPhotos(
                get<PhotoCatalogRepository>()
        )
    }

    factory {
        GetActualPage(
                get<PagingRepository>()
        )
    }

    factory {
        SetActualPage(
                get<PagingRepository>()
        )
    }

    factory {
        GetActualSearchText(
                get<SearchTextRepository>()
        )
    }

    factory {
        SetActualSearchText(
                get<SearchTextRepository>()
        )
    }

    factory {
        GetIfIsTheUsersFirstTime(
                get<UserFirstTimeRepository>()
        )
    }

    factory {
        SetThatIsNotTheUsersFirstTime(
                get<UserFirstTimeRepository>()
        )
    }

    factory {
        UserFirstTimeRepositoryImpl(
                get<UserFirstTimeCache>(),
                androidContext()
        ) as UserFirstTimeRepository
    }

    factory {
        SearchTextRepositoryImpl(
                get<SearchTextCache>(),
                androidContext()
        ) as SearchTextRepository
    }

    factory {
        PhotoCatalogRepositoryImpl(
                get<PhotoCatalogService>(),
                get<PhotoDao>()
        ) as PhotoCatalogRepository
    }

    factory {
        PagingRepositoryImpl(
                get<PagingCache>(),
                androidContext()

        ) as PagingRepository
    }

    factory {
        UserFirstTimeCache()
    }

    factory {
        PagingCache()
    }

    factory {
        SearchTextCache()
    }

    // <-- Retrofit setup init -->
    factory {
        createOkHttpClient()
    }

    factory {
        createRetrofit(
                get()
        )
    }

    factory {
        getMatchApiService(
                get<Retrofit>()
        )
    }
    // <-- Retrofit setup end -->

    // <-- Database setup init -->
    single {
        Room.databaseBuilder(androidContext(), PhotoDatabase::class.java, PHOTO_DATABASE)
                .allowMainThreadQueries()
                .build()
    }

    single {
        get<PhotoDatabase>().photoDao()
    }
    // <-- Database setup end -->
}

fun createOkHttpClient(): OkHttpClient {
    val timeoutInSeconds = 10L
    val httpLoggingInterceptor = HttpLoggingInterceptor()
    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC
    return OkHttpClient.Builder()
            .connectTimeout(timeoutInSeconds, TimeUnit.SECONDS)
            .readTimeout(timeoutInSeconds, TimeUnit.SECONDS)
            .addInterceptor(httpLoggingInterceptor).build()
}

fun createRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
        .baseUrl(PHOTO_CATALOG_API_BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()

fun getMatchApiService(retrofit: Retrofit): PhotoCatalogService =
        retrofit.create(PhotoCatalogService::class.java)
