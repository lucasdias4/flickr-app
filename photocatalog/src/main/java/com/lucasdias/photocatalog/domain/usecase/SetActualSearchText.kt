package com.lucasdias.photocatalog.domain.usecase

import com.lucasdias.photocatalog.domain.repository.SearchTextRepository

class SetActualSearchText(
    private var searchTextRepository: SearchTextRepository
) {
    fun execute(searchText: String) {
        searchTextRepository.setActualSearchText(searchText)
    }
}
