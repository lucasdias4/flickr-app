package com.lucasdias.photocatalog.domain.repository

import androidx.lifecycle.LiveData
import com.lucasdias.photocatalog.domain.enums.RequestStatusEnum
import com.lucasdias.photocatalog.domain.model.PhotoModel

interface PhotoCatalogRepository {
    suspend fun searchPhotosByTagAndPage(tag: String, page: Int, apiKey: String): RequestStatusEnum
    fun getAllPhotos(): LiveData<List<PhotoModel>>
    fun requestError(): LiveData<Unit>
    fun deleteAllPhoto()
}
