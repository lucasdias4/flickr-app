package com.lucasdias.photocatalog.domain.usecase

import com.lucasdias.photocatalog.domain.repository.SearchTextRepository

class GetActualSearchText(
    private var searchTextRepository: SearchTextRepository
) {
    fun execute(): String {
        val actualSearchText = searchTextRepository.getActualSearchText()
        return actualSearchText
    }
}
