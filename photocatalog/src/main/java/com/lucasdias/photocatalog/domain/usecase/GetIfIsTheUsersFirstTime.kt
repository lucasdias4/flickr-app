package com.lucasdias.photocatalog.domain.usecase

import com.lucasdias.photocatalog.domain.repository.UserFirstTimeRepository

class GetIfIsTheUsersFirstTime(
    private var userFirstTimeRepository: UserFirstTimeRepository
) {
    fun execute(): Boolean {
        return userFirstTimeRepository.getIfIsUserFirstTime()
    }
}
