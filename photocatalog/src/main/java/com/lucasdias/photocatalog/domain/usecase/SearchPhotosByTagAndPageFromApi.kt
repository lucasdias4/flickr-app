package com.lucasdias.photocatalog.domain.usecase

import com.lucasdias.photocatalog.domain.enums.RequestStatusEnum
import com.lucasdias.photocatalog.domain.repository.PhotoCatalogRepository

class SearchPhotosByTagAndPageFromApi(
    private val photoCatalogRepository: PhotoCatalogRepository
) {

    suspend fun execute(tag: String, page: Int, apiKey: String): RequestStatusEnum {
        return photoCatalogRepository.searchPhotosByTagAndPage(tag, page, apiKey)
    }
}
