package com.lucasdias.photocatalog.domain.usecase

import com.lucasdias.photocatalog.domain.repository.PagingRepository

class GetActualPage(
    private var pagingRepository: PagingRepository
) {
    fun execute() = pagingRepository.getActualPage()
}
