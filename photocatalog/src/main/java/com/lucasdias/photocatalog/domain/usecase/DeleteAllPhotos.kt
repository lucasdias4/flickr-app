package com.lucasdias.photocatalog.domain.usecase

import com.lucasdias.photocatalog.domain.repository.PhotoCatalogRepository

class DeleteAllPhotos(
    private val photoCatalogRepository: PhotoCatalogRepository
) {
    fun execute() {
        photoCatalogRepository.deleteAllPhoto()
    }
}
