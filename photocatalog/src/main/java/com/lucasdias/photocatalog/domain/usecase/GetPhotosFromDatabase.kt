package com.lucasdias.photocatalog.domain.usecase

import androidx.lifecycle.LiveData
import com.lucasdias.photocatalog.domain.model.PhotoModel
import com.lucasdias.photocatalog.domain.repository.PhotoCatalogRepository

class GetPhotosFromDatabase(
    private val photoCatalogRepository: PhotoCatalogRepository
) {
    fun execute(): LiveData<List<PhotoModel>> {
        return photoCatalogRepository.getAllPhotos()
    }
}
