package com.lucasdias.photocatalog.domain.enums

enum class RequestStatusEnum(var id: Int) {
    ERROR(0),
    SUCCESS_WITHOUT_PHOTOS_ON_FIRST_SEARCH(1),
    SUCCESS_WITHOUT_PHOTOS_NOT_IN_THE_FIRST_SEARCH(2),
    SUCCESS_WITH_PHOTOS(3)
}
