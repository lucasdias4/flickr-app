package com.lucasdias.photocatalog.domain.usecase

import com.lucasdias.photocatalog.domain.repository.PagingRepository

class SetActualPage(
    private var pagingRepository: PagingRepository
) {
    fun execute(page: Int) {
        pagingRepository.setActualPage(page)
    }
}
