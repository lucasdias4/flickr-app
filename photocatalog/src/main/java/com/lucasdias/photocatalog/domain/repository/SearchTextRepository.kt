package com.lucasdias.photocatalog.domain.repository

interface SearchTextRepository {
    fun setActualSearchText(searchText: String)
    fun getActualSearchText(): String
}
