package com.lucasdias.photocatalog.domain.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "photo")
data class PhotoModel(
    @PrimaryKey
    @ColumnInfo(name = "id") var id: String,
    @ColumnInfo(name = "title") var title: String,
    @ColumnInfo(name = "smallPhotoUrl") var smallPhotoUrl: String,
    @ColumnInfo(name = "mediumPhotoUrl") var mediumPhotoUrl: String,
    @ColumnInfo(name = "bigPhotoUrl") var bigPhotoUrl: String
)
