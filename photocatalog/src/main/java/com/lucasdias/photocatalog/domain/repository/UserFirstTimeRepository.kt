package com.lucasdias.photocatalog.domain.repository

interface UserFirstTimeRepository {
    fun getIfIsUserFirstTime(): Boolean
    fun setThatIsNotTheUsersFirstTime()
}
