package com.lucasdias.photocatalog.domain.repository

interface PagingRepository {
    fun getActualPage(): Int
    fun setActualPage(page: Int)
}
