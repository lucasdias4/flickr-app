package com.lucasdias.photocatalog.domain.usecase

import com.lucasdias.photocatalog.domain.repository.UserFirstTimeRepository

class SetThatIsNotTheUsersFirstTime(
    private var userFirstTimeRepository: UserFirstTimeRepository
) {
    fun execute() {
        userFirstTimeRepository.setThatIsNotTheUsersFirstTime()
    }
}
