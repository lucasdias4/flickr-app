package com.lucasdias.photocatalog.utils

class Constants {
    companion object {
        const val API_KEY = "f9cc014fa76b098f9e82f1c288379ea1"
        const val PHOTO_BASE_URL = "https://live.staticflickr.com"
        const val PHOTO_CATALOG_API_BASE_URL = "https://api.flickr.com"
        const val PHOTO_DATABASE = "PHOTO_DATABASE"
    }
}
