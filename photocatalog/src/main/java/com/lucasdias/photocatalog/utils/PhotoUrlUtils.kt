package com.lucasdias.photocatalog.utils

import com.lucasdias.photocatalog.utils.Constants.Companion.PHOTO_BASE_URL

class PhotoUrlUtils {
    companion object {

        fun getAllSizesPhotoUrl(
            serverId: String,
            photoId: String,
            secretId: String
        ): ArrayList<String> {

            val photoUrls = ArrayList<String>()

            val smallPhotoUrl = getSmallPhotoUrl(serverId, photoId, secretId)
            val mediumPhotoUrl = getMediumPhotoUrl(serverId, photoId, secretId)
            val bigPhotoUrl = getBigPhotoUrl(serverId, photoId, secretId)

            photoUrls.add(smallPhotoUrl)
            photoUrls.add(mediumPhotoUrl)
            photoUrls.add(bigPhotoUrl)

            return photoUrls
        }

        fun getSmallPhotoUrl(
            serverId: String,
            photoId: String,
            secretId: String
        ): String {
            val posFix = "q.jpg"
            return "$PHOTO_BASE_URL/$serverId/${photoId}_${secretId}_$posFix"
        }

        fun getMediumPhotoUrl(
            serverId: String,
            photoId: String,
            secretId: String
        ): String {
            val posFix = ".jpg"
            return "$PHOTO_BASE_URL/$serverId/${photoId}_${secretId}$posFix"
        }

        fun getBigPhotoUrl(
            serverId: String,
            photoId: String,
            secretId: String
        ): String {
            val posFix = "b.jpg"
            return "$PHOTO_BASE_URL/$serverId/${photoId}_${secretId}_$posFix"
        }
    }
}
