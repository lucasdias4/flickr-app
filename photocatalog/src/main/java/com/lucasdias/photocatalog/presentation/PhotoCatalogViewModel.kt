package com.lucasdias.photocatalog.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.GridLayoutManager
import com.lucasdias.photocatalog.domain.enums.RequestStatusEnum
import com.lucasdias.photocatalog.domain.enums.RequestStatusEnum.*
import com.lucasdias.photocatalog.domain.model.PhotoModel
import com.lucasdias.photocatalog.domain.usecase.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PhotoCatalogViewModel(
    private val searchPhotosByTagAndPageFromApi: SearchPhotosByTagAndPageFromApi,
    private val getPhotosFromDatabase: GetPhotosFromDatabase,
    private val getActualPage: GetActualPage,
    private val setActualPage: SetActualPage,
    private val getActualSearchText: GetActualSearchText,
    private val setActualSearchText: SetActualSearchText,
    private val deleteAllPhotos: DeleteAllPhotos,
    private val getIfIsTheUsersFirstTime: GetIfIsTheUsersFirstTime,
    private val setThatIsNotTheUsersFirstTime: SetThatIsNotTheUsersFirstTime
) : ViewModel() {

    companion object {
        private const val NONE_ITEM = 0
    }

    internal var needToFetchPhotosByTagAndPage = MutableLiveData<Unit>()
    internal var isLoadingDataFromService = MutableLiveData<Unit>()
    internal var networkError = MutableLiveData<Unit>()
    internal var isUserFirstTime = MutableLiveData<Unit>()
    internal var isUserNotFirstTime = MutableLiveData<Unit>()
    internal var requestError = MutableLiveData<Unit>()
    internal var emptySearchText = MutableLiveData<Unit>()
    internal var photoNotFound = MutableLiveData<Unit>()
    internal var updatePhotoListOnView = MutableLiveData<List<PhotoModel>>()
    internal var needToClearPhotoCatalog = MutableLiveData<Unit>()

    fun needToFetchPhotosByTagAndPage(): LiveData<Unit> = needToFetchPhotosByTagAndPage
    fun isLoadingDataFromService(): LiveData<Unit> = isLoadingDataFromService
    fun photoList(): LiveData<List<PhotoModel>> = getPhotosFromDatabase.execute()
    fun requestError(): LiveData<Unit> = requestError
    fun networkError(): LiveData<Unit> = networkError
    fun isUserFirstTime(): LiveData<Unit> = isUserFirstTime
    fun isUserNotFirstTime(): LiveData<Unit> = isUserNotFirstTime
    fun photoNotFound(): LiveData<Unit> = photoNotFound
    fun emptySearchText(): LiveData<Unit> = emptySearchText
    fun updatePhotoListOnView(): LiveData<List<PhotoModel>> = updatePhotoListOnView
    fun needToClearPhotoCatalog(): LiveData<Unit> = needToClearPhotoCatalog

    fun fetchPhotosByTagAndPage(
        searchText: String,
        actualPage: Int,
        hasNetworkConnectivity: Boolean,
        apiKey: String
    ) {

        setThatIsNotTheUsersFirstTime.execute()

        if (!hasNetworkConnectivity) {
            networkError.postValue(Unit)
            return
        }

        if (searchText.isEmpty()) {
            emptySearchText.postValue(Unit)
            return
        }

        CoroutineScope(Dispatchers.IO).launch {
            isLoadingDataFromService.postValue(Unit)
            val requestStatus =
                searchPhotosByTagAndPageFromApi.execute(searchText, actualPage, apiKey)
            requestStatusHandler(requestStatus)
        }
    }

    fun setActualPage(page: Int) {
        setActualPage.execute(page)
    }

    fun getActualPage(): Int {
        return getActualPage.execute()
    }

    fun setActualSearchText(searchText: String) {
        setActualSearchText.execute(searchText)
    }

    fun getActualSearchText(): String {
        return getActualSearchText.execute()
    }

    fun deleteAllPhotos() {
        deleteAllPhotos.execute()
    }

    fun needToUpdatePhotoListOnView(photoList: List<PhotoModel>?) {
        if (photoList.isNullOrEmpty().not()) updatePhotoListOnView.postValue(photoList)
        else needToClearPhotoCatalog.postValue(Unit)
    }

    fun verifyIfItIsTheUsersFirstTime() {
        val firstTime = getIfIsTheUsersFirstTime.execute()

        if (firstTime) isUserFirstTime.postValue(Unit)
        else isUserNotFirstTime.postValue(Unit)
    }

    fun onRecyclerViewScrolled(
        dy: Int,
        isLoading: Boolean,
        layoutManager: GridLayoutManager
    ) {
        if (dy > NONE_ITEM) {
            val itIsTheListEnd = itIsTheListEnd(layoutManager)
            if (isLoading.not() && itIsTheListEnd) {
                needToFetchPhotosByTagAndPage.postValue(Unit)
            }
        }
    }

    internal fun itIsTheListEnd(layoutManager: GridLayoutManager): Boolean {
        val visibleItemCount = layoutManager.childCount
        val totalItemCount = layoutManager.itemCount
        val pastVisiblesItems = layoutManager.findFirstVisibleItemPosition()
        return visibleItemCount + pastVisiblesItems >= totalItemCount
    }

    private fun requestStatusHandler(requestStatus: RequestStatusEnum) {
        when (requestStatus) {
            ERROR -> {
                requestError.postValue(Unit)
            }
            SUCCESS_WITHOUT_PHOTOS_ON_FIRST_SEARCH -> {
                photoNotFound.postValue(Unit)
            }
            SUCCESS_WITHOUT_PHOTOS_NOT_IN_THE_FIRST_SEARCH -> {
                photoNotFound.postValue(Unit)
            }
            SUCCESS_WITH_PHOTOS -> {
            }
        }
    }
}
