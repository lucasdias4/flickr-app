package com.lucasdias.photocatalog.presentation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.lucasdias.baseextensions.bind
import com.lucasdias.baseextensions.loadUrl
import com.lucasdias.photocatalog.R
import com.lucasdias.photocatalog.domain.model.PhotoModel

class PhotoCatalogAdapter(private val photoClickMethod: ((String) -> Unit)?) :
        RecyclerView.Adapter<PhotoCatalogAdapter.ViewHolder>() {

    private var photoCatalog = mutableListOf<PhotoModel>()

    fun updatePhotoCatalog(photoCatalog: List<PhotoModel>) {
        if (this.photoCatalog.isNotEmpty()) {
            this.photoCatalog.clear()
        }

        this.photoCatalog.addAll(photoCatalog)
        notifyDataChange()
    }

    fun clearPhotoCatalog() {
        this.photoCatalog.clear()
        notifyDataChange()
    }

    fun notifyDataChange() {
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewLayout =
                LayoutInflater.from(parent.context).inflate(R.layout.photo_item, parent, false)
        return ViewHolder(viewLayout)
    }

    override fun getItemCount(): Int {
        return photoCatalog.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (photoCatalog.isNotEmpty()) {
            holder.itemBind(photoCatalog[position], photoClickMethod)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val photoImageView by bind<ImageView>(itemView, R.id.photo_item_image)

        fun itemBind(photo: PhotoModel, photoClickMethod: ((String) -> Unit)?) {
            photoImageView.loadUrl(photo.smallPhotoUrl)
            itemView.setOnClickListener {
                photoClickMethod?.let { method ->
                    method(photo.bigPhotoUrl)
                }
            }
        }
    }
}
