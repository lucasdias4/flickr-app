package com.lucasdias.photocatalog.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.lucasdias.baseextensions.*
import com.lucasdias.photocatalog.R
import com.lucasdias.photocatalog.utils.Constants.Companion.API_KEY
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class PhotoCatalogFragment(private val photoClickMethod: ((String) -> Unit)?) : Fragment() {

    companion object {
        private const val FIRST_PAGE = 1
        private const val LIST_WITH_TWO_ITEMS_PER_ROW = 2
        private const val SINGLE_PAGE = 1
        fun newInstance(photoClickMethod: ((String) -> Unit)?) =
            PhotoCatalogFragment(photoClickMethod)
    }

    var isLoading = false
    private var hasNetworkConnectivity = true
    private val viewModel by viewModel<PhotoCatalogViewModel>()
    private val adapter: PhotoCatalogAdapter by inject { parametersOf(photoClickMethod) }
    private val swipeRefresh by bind<SwipeRefreshLayout>(R.id.photo_fragment_swipe_refresh_layout)
    private val recyclerView by bind<RecyclerView>(R.id.photo_fragment_photo_catalog_recycler_view)
    private val loadingView by bind<View>(R.id.photo_fragment_loading_item)
    private val message by bind<TextView>(R.id.photo_fragment_message)
    private lateinit var layoutManager: GridLayoutManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_photo_catalog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
        initRecyclerView()
        initSwipeRefresh()
        verifyIfItIsTheUsersFirstTime()
    }

    override fun onStop() {
        super.onStop()
        activity?.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }

    fun updateSearch(searchText: String) {
        viewModel.setActualPage(FIRST_PAGE)
        val actualPage = viewModel.getActualPage()
        val isNetworkAvailable = context?.isNetworkAvailable() ?: false
        viewModel.apply {
            deleteAllPhotos()
            setActualSearchText(searchText)
            fetchPhotosByTagAndPage(searchText, actualPage, isNetworkAvailable, API_KEY)
        }
    }

    fun verifyIfItIsTheUsersFirstTime() {
        viewModel.verifyIfItIsTheUsersFirstTime()
    }

    fun updateConnectivityInformation(hasNetworkConnectivity: Boolean) {
        this.hasNetworkConnectivity = hasNetworkConnectivity
    }

    private fun initSwipeRefresh() {
        swipeRefresh?.setOnRefreshListener {
            viewModel.apply {
                val actualSearchText = getActualSearchText()
                viewModel.setActualPage(FIRST_PAGE)
                fetchPhotosByTagAndPage(
                    actualSearchText,
                    FIRST_PAGE,
                    hasNetworkConnectivity,
                    API_KEY
                )
            }
        }
    }

    private fun initObservers() {
        viewModel.apply {
            photoList().observe(viewLifecycleOwner, Observer { photoList ->
                turnOffRefreshSwipe()
                viewModel.needToUpdatePhotoListOnView(photoList)
            })

            updatePhotoListOnView().observe(viewLifecycleOwner, Observer { photoList ->
                message?.gone()
                adapter.updatePhotoCatalog(photoList)
                isNotLoading()
            })

            isLoadingDataFromService().observe(viewLifecycleOwner, Observer {
                isLoading()
            })

            needToClearPhotoCatalog().observe(viewLifecycleOwner, Observer {
                adapter.clearPhotoCatalog()
            })

            networkError().observe(viewLifecycleOwner, Observer {
                val errorMessage: CharSequence =
                    context?.getString(R.string.photo_catalog_request_error_message) as CharSequence
                isNotLoading()
                message?.gone()
                context?.toast(errorMessage)
            })

            requestError().observe(viewLifecycleOwner, Observer {
                val errorMessage: CharSequence =
                    context?.getString(R.string.photo_catalog_request_error_message) as CharSequence
                isNotLoading()
                message?.gone()
                context?.toast(errorMessage)
            })

            photoNotFound().observe(viewLifecycleOwner, Observer {
                message?.text = context?.getString(R.string.photo_catalog_photo_not_found)
                isNotLoading()
                message?.visible()
            })

            isUserFirstTime().observe(viewLifecycleOwner, Observer {
                message?.text = context?.getString(R.string.photo_catalog_welcome_message)
                message?.visible()
            })

            isUserNotFirstTime().observe(viewLifecycleOwner, Observer {
                message?.gone()
            })

            emptySearchText().observe(viewLifecycleOwner, Observer {
                val emptySearchText: CharSequence =
                    context?.getString(R.string.photo_catalog_search_text_is_empty) as CharSequence
                context?.toast(emptySearchText)
            })

            needToFetchPhotosByTagAndPage().observe(viewLifecycleOwner, Observer {
                val actualSearchText = viewModel.getActualSearchText()
                val oldPage = getActualPage()
                val newActualPage = oldPage + SINGLE_PAGE
                setActualPage(newActualPage)
                fetchPhotosByTagAndPage(
                    actualSearchText,
                    newActualPage,
                    hasNetworkConnectivity,
                    API_KEY
                )
            })
        }
    }

    private fun initRecyclerView() {
        layoutManager = GridLayoutManager(context, LIST_WITH_TWO_ITEMS_PER_ROW)
        recyclerView?.setHasFixedSize(true)
        recyclerView?.layoutManager = layoutManager
        recyclerView?.adapter = adapter
        recyclerViewScrollListener(recyclerView)
    }

    private fun recyclerViewScrollListener(recyclerView: RecyclerView?) {
        recyclerView?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                viewModel.onRecyclerViewScrolled(dy, isLoading, layoutManager)
            }
        })
    }

    private fun turnOffRefreshSwipe(){
        swipeRefresh?.isRefreshing = false
    }

    private fun isLoading() {
        isLoading = true
        loadingView?.visible()
    }

    private fun isNotLoading() {
        isLoading = false
        loadingView?.gone()
    }
}
