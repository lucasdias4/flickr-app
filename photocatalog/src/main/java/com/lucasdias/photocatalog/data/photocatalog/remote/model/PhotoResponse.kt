package com.lucasdias.photocatalog.data.photocatalog.remote.model

import com.google.gson.annotations.SerializedName

class PhotoResponse(
    @SerializedName("id") var id: String,
    @SerializedName("owner") var owner: String,
    @SerializedName("secret") var secret: String,
    @SerializedName("server") var server: String,
    @SerializedName("farm") var farm: Int,
    @SerializedName("title") var title: String,
    @SerializedName("ispublic") var isPublic: Int,
    @SerializedName("isfriend") var isFriend: Int,
    @SerializedName("isfamily") var isFamily: Int
)
