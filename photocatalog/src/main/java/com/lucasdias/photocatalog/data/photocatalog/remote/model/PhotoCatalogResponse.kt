package com.lucasdias.photocatalog.data.photocatalog.remote.model

import com.google.gson.annotations.SerializedName

data class PhotoCatalogResponse(
    @SerializedName("photos") var page: PhotoPageResponse,
    @SerializedName("stat") var status: String
)
