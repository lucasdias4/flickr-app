package com.lucasdias.photocatalog.data.photocatalog

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.lucasdias.photocatalog.data.photocatalog.local.PhotoDao
import com.lucasdias.photocatalog.data.photocatalog.mapper.PhotoMapper
import com.lucasdias.photocatalog.data.photocatalog.remote.PhotoCatalogService
import com.lucasdias.photocatalog.domain.enums.RequestStatusEnum
import com.lucasdias.photocatalog.domain.enums.RequestStatusEnum.*
import com.lucasdias.photocatalog.domain.model.PhotoModel
import com.lucasdias.photocatalog.domain.repository.PhotoCatalogRepository
import kotlinx.coroutines.withTimeout

class PhotoCatalogRepositoryImpl(
    private val photoCatalogSevice: PhotoCatalogService,
    private val photoDao: PhotoDao
) : PhotoCatalogRepository {

    companion object {
        private const val FIRST_REQUEST = 1
        private const val MIN_RESPONSE_CODE = 200
        private const val MAX_RESPONSE_CODE = 299
        private const val REQUEST_TIMEOUT = 10000L
    }

    private val requestError = MutableLiveData<Unit>()

    override fun requestError(): LiveData<Unit> = requestError
    override fun getAllPhotos(): LiveData<List<PhotoModel>> = photoDao.getAllPhotos()

    override fun deleteAllPhoto() {
        photoDao.deleteAllPhotos()
    }

    override suspend fun searchPhotosByTagAndPage(
        tag: String,
        page: Int,
        apiKey: String
    ): RequestStatusEnum {
        val requestStatus = withTimeout(REQUEST_TIMEOUT) {
            try {
                val photoCatalogResponse =
                    photoCatalogSevice.fetchPhotosByTagAndPage(tag, page, apiKey).await()

                val responseCode = photoCatalogResponse.code()

                if (responseCode in MIN_RESPONSE_CODE..MAX_RESPONSE_CODE) {

                    val photoList = photoCatalogResponse.body()?.page?.photoCatalog
                    val responseIsNotEmpty: Boolean = photoList?.isNotEmpty() ?: false

                    if (responseIsNotEmpty) {
                        val photoListDataModel =
                            PhotoMapper.dataResponseToDomainModel(photoList!!)
                        insertPhotosInDatabase(photoListDataModel)
                        return@withTimeout SUCCESS_WITH_PHOTOS
                    } else if (page == FIRST_REQUEST) {
                        return@withTimeout SUCCESS_WITHOUT_PHOTOS_ON_FIRST_SEARCH
                    } else {
                        return@withTimeout SUCCESS_WITHOUT_PHOTOS_NOT_IN_THE_FIRST_SEARCH
                    }
                } else {
                    return@withTimeout ERROR
                }
            } catch (exception: Exception) {
                return@withTimeout ERROR
            }
        }
        return requestStatus
    }

    private fun insertPhotosInDatabase(photoListDataModel: ArrayList<PhotoModel>) {
        photoDao.insertPhotos(photoListDataModel)
    }
}
