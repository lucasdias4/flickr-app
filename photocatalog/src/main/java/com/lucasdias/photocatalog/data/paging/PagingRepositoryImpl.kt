package com.lucasdias.photocatalog.data.paging

import android.content.Context
import com.lucasdias.photocatalog.data.paging.local.PagingCache
import com.lucasdias.photocatalog.domain.repository.PagingRepository

class PagingRepositoryImpl(
    private var pagingCache: PagingCache,
    private var context: Context
) : PagingRepository {

    override fun getActualPage(): Int {
        return pagingCache.getActualPage(context)
    }

    override fun setActualPage(page: Int) {
        pagingCache.setActualPage(page, context)
    }
}
