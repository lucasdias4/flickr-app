package com.lucasdias.photocatalog.data.paging.local

import android.content.Context
import android.preference.PreferenceManager

class PagingCache {

    private companion object {
        private const val PAGE = "PAGE"
        private const val DEFAULT_VALUE = 1
    }

    fun setActualPage(page: Int, context: Context) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = preferences.edit()
        editor.putInt(PAGE, page)
        editor.apply()
    }

    fun getActualPage(context: Context): Int {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getInt(PAGE, DEFAULT_VALUE)
    }
}
