package com.lucasdias.photocatalog.data.photocatalog.remote

import com.lucasdias.photocatalog.data.photocatalog.remote.model.PhotoCatalogResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface PhotoCatalogService {

    @GET("/services/rest/?method=flickr.photos.search&format=json&nojsoncallback=1")
    fun fetchPhotosByTagAndPage(
        @Query(value = "tags") tag: String,
        @Query(value = "page") page: Int,
        @Query(value = "api_key") apiKey: String
    ): Deferred<Response<PhotoCatalogResponse>>
}
