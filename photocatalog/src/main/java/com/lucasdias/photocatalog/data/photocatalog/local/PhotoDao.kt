package com.lucasdias.photocatalog.data.photocatalog.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.lucasdias.photocatalog.domain.model.PhotoModel

@Dao
interface PhotoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPhotos(photos: List<PhotoModel>)

    @Query("SELECT * FROM photo")
    fun getAllPhotos(): LiveData<List<PhotoModel>>

    @Query("DELETE FROM photo")
    fun deleteAllPhotos()
}
