package com.lucasdias.photocatalog.data.photocatalog.remote.model

import com.google.gson.annotations.SerializedName

data class PhotoPageResponse(
    @SerializedName("page") var page: Int,
    @SerializedName("pages") var pages: Int,
    @SerializedName("total") var total: Long,
    @SerializedName("photo") var photoCatalog: ArrayList<PhotoResponse>
)
