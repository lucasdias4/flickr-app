package com.lucasdias.photocatalog.data.photocatalog.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.lucasdias.photocatalog.domain.model.PhotoModel

@Database(version = 1, entities = [PhotoModel::class])

abstract class PhotoDatabase : RoomDatabase() {
    abstract fun photoDao(): PhotoDao
}
