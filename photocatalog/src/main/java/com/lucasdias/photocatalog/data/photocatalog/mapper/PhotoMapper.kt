package com.lucasdias.photocatalog.data.photocatalog.mapper

import com.lucasdias.photocatalog.data.photocatalog.remote.model.PhotoResponse
import com.lucasdias.photocatalog.domain.model.PhotoModel
import com.lucasdias.photocatalog.utils.PhotoUrlUtils

class PhotoMapper {
    companion object {
        const val SMALL_SIZE = 0
        const val MEDIUM_SIZE = 1
        const val LARGE_SIZE = 2

        fun dataResponseToDomainModel(photoDataResponse: PhotoResponse): PhotoModel {

            val photoUrls = PhotoUrlUtils.getAllSizesPhotoUrl(
                    photoDataResponse.server,
                    photoDataResponse.id,
                    photoDataResponse.secret
            )

            val photoDomainModel = PhotoModel(
                    photoDataResponse.id,
                    photoDataResponse.title,
                    photoUrls[SMALL_SIZE],
                    photoUrls[MEDIUM_SIZE],
                    photoUrls[LARGE_SIZE]
            )
            return photoDomainModel
        }

        fun dataResponseToDomainModel(
            photoListDataResponse: ArrayList<PhotoResponse>
        ): ArrayList<PhotoModel> {

            val photoDataModelList = ArrayList<PhotoModel>()

            photoListDataResponse.forEach { photoDataResponse ->

                val photoUrls = PhotoUrlUtils.getAllSizesPhotoUrl(
                        photoDataResponse.server,
                        photoDataResponse.id,
                        photoDataResponse.secret
                )

                val photoDomainModel = PhotoModel(
                        photoDataResponse.id,
                        photoDataResponse.title,
                        photoUrls[SMALL_SIZE],
                        photoUrls[MEDIUM_SIZE],
                        photoUrls[LARGE_SIZE]
                )

                photoDataModelList.add(photoDomainModel)
            }
            return photoDataModelList
        }
    }
}
