package com.lucasdias.photocatalog.data.userfirsttime

import android.content.Context
import com.lucasdias.photocatalog.data.userfirsttime.local.UserFirstTimeCache
import com.lucasdias.photocatalog.domain.repository.UserFirstTimeRepository

class UserFirstTimeRepositoryImpl(
    private var userFirstTimeCache: UserFirstTimeCache,
    private var context: Context
) : UserFirstTimeRepository {
    override fun getIfIsUserFirstTime(): Boolean {
        return userFirstTimeCache.getIfIsUserFirstTime(context)
    }

    override fun setThatIsNotTheUsersFirstTime() {
        userFirstTimeCache.setIsNotFirstTime(context)
    }
}
