package com.lucasdias.photocatalog.presentation

import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.GridLayoutManager
import com.lucasdias.photocatalog.domain.usecase.*
import io.mockk.*
import org.junit.Test

class PhotoCatalogViewModelTest {

    private companion object {
        const val ZERO_NUMBER = 0
        const val POSITIVE_NUMBER = 1
        const val IS_LOADING = true
        const val IS_NOT_LOADING = false
    }

    private val searchPhotosByTagAndPageFromApi: SearchPhotosByTagAndPageFromApi = mockk()
    private val getPhotosFromDatabase: GetPhotosFromDatabase = mockk()
    private val getActualPage: GetActualPage = mockk()
    private val setActualPage: SetActualPage = mockk()
    private val getActualSearchText: GetActualSearchText = mockk()
    private val setActualSearchText: SetActualSearchText = mockk()
    private val deleteAllPhotos: DeleteAllPhotos = mockk()
    private val getIfIsTheUsersFirstTime: GetIfIsTheUsersFirstTime = mockk()
    private val setThatIsNotTheUsersFirstTime: SetThatIsNotTheUsersFirstTime = mockk()
    private val mockedLayoutManager: GridLayoutManager = mockk()
    private val photoCatalogViewModel = spyk(
        PhotoCatalogViewModel(
            searchPhotosByTagAndPageFromApi,
            getPhotosFromDatabase,
            getActualPage,
            setActualPage,
            getActualSearchText,
            setActualSearchText,
            deleteAllPhotos,
            getIfIsTheUsersFirstTime,
            setThatIsNotTheUsersFirstTime
        )
    )

    @Test
    fun `GIVEN that the user scroll the list WHEN variable dy it is bigger than zero THEN calls next validation`() {
        every {
            photoCatalogViewModel.itIsTheListEnd(mockedLayoutManager)
        } returns false

        photoCatalogViewModel.onRecyclerViewScrolled(
            POSITIVE_NUMBER,
            IS_NOT_LOADING,
            mockedLayoutManager
        )

        verify(exactly = 1) {
            photoCatalogViewModel.itIsTheListEnd(any())
        }
    }

    @Test
    fun `GIVEN that the user scroll the list WHEN variable dy it is equal than zero THEN do not call next validation`() {
        every {
            photoCatalogViewModel.itIsTheListEnd(mockedLayoutManager)
        } returns true

        photoCatalogViewModel.onRecyclerViewScrolled(
            ZERO_NUMBER,
            IS_NOT_LOADING,
            mockedLayoutManager
        )

        verify(exactly = 0) {
            photoCatalogViewModel.itIsTheListEnd(any())
        }
    }

    @Test
    fun `GIVEN the user scrolls the list WHEN the dy variable is positive and the application is not requesting API data THEN make a request to the API`() {
        val mockedNeedToFetchPhotosByTagAndPage: MutableLiveData<Unit> = mockk()
        photoCatalogViewModel.needToFetchPhotosByTagAndPage = mockedNeedToFetchPhotosByTagAndPage

        every {
            photoCatalogViewModel.itIsTheListEnd(mockedLayoutManager)
        } returns true

        every {
            mockedNeedToFetchPhotosByTagAndPage.postValue(Unit)
        } just Runs

        photoCatalogViewModel.onRecyclerViewScrolled(
            POSITIVE_NUMBER,
            IS_NOT_LOADING,
            mockedLayoutManager
        )

        verify(exactly = 1) {
            mockedNeedToFetchPhotosByTagAndPage.postValue(any())
        }
    }

    @Test
    fun `GIVEN the user scrolls the list WHEN the dy variable is positive and the application is requesting API data THEN do not make a request to the API`() {
        val mockedNeedToFetchPhotosByTagAndPage: MutableLiveData<Unit> = mockk()
        photoCatalogViewModel.needToFetchPhotosByTagAndPage = mockedNeedToFetchPhotosByTagAndPage

        every {
            photoCatalogViewModel.itIsTheListEnd(mockedLayoutManager)
        } returns true

        every {
            mockedNeedToFetchPhotosByTagAndPage.postValue(Unit)
        } just Runs

        photoCatalogViewModel.onRecyclerViewScrolled(
            POSITIVE_NUMBER,
            IS_LOADING,
            mockedLayoutManager
        )

        verify(exactly = 0) {
            mockedNeedToFetchPhotosByTagAndPage.postValue(any())
        }
    }
}
